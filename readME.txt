#HOW TO MAKE THE CODE WORK ON YOUR COMPUTER:

1) In your computer, create a folder on your preferred working directory.
   Example: 
   i) Go to 'C:\Users\Mayette\Desktop\RProjects'
   ii) Right click, select "New" and click "Folder"
   iii) Rename the folder with your preferred name (this will now be your working directory)

2) Open your working directory and create the following subfolders: "/downloads" and "/source"

3) Open RStudio. Create new files (R Script): 

   i) Copy the R script from this link and save it as 'delivery.R' in your working directory:
      https://bitbucket.org/missusmilady/psc-kitchen-stuff/src/master/delivery.R

   ii) Copy the R script from this link and save it as 'compile.R' in the "/source" subfolder:
       https://bitbucket.org/missusmilady/psc-kitchen-stuff/src/master/source/compile.R

   iii) Copy the R script from this link and save it as 'download.R' in the "/source" subfolder:
	https://bitbucket.org/missusmilady/psc-kitchen-stuff/src/master/source/download.R

   iv) Copy the R script from this link and save it as 'folder.R' in the "/source" subfolder:
       https://bitbucket.org/missusmilady/psc-kitchen-stuff/src/master/source/folder.R

   v) Copy the R script from this link and save it as 'page.R' in the "/source" subfolder:
      https://bitbucket.org/missusmilady/psc-kitchen-stuff/src/master/source/page.R

   vi) Copy the code from this link and save it as 'unprocessed.R' in the "/source" subfolder":
       https://bitbucket.org/missusmilady/psc-kitchen-stuff/src/master/source/unprocessed.R

4) Download 'url.csv' file from this link and save it in your working directory:
   https://bitbucket.org/missusmilady/psc-kitchen-stuff/src/53c66fc644d6?at=master
	
5) Run 'delivery.R' code per chunk (denoted by "#" partitions).
   Tweak the code as needed (e.g. #SET YOUR WORKING DIRECTORY).


####NOTHING FOLLOWS