kitchen <- "Monumento"

folder <- paste(getwd(), "downloads", "monumento", sep="/")

hour <- c(5:18)

wb <- list.files(path = folder, pattern = '*.xlsx', all.files = FALSE,
                 full.names = FALSE, recursive = FALSE,
                 ignore.case = FALSE, include.dirs = FALSE, no.. = FALSE)

datalist <- list()

for (i in 1:length(wb)) {
  
  check_time <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "E1:E2") %>% as.data.frame()
  
  check_col <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "IK1:IK2") %>% as.data.frame()
  
  check_row <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "D84:D85") %>% as.data.frame()
  
  if (check_time[1,]=='4:30AM - 5:00AM' && 
      check_col[1,]=='TOTAL SERVED' && 
      check_row[1,]=='Mktg-Food Service') {
    
      source(paste(getwd(), "source", "format", "format4.R", sep="/"))
    
  }
  
  datalist[[i]] <- daily  
  
}

alldata <- do.call(rbind, datalist) %>% filter(qty > 0) %>% na.omit()

alldata$kitchen <- kitchen

monumento <- alldata[,c("kitchen", "date", "hour", "store_code", 'item', "qty")]

###NOTHING FOLLOWS