#KITCHEN DELIVERY FILES: FORMAT 5

row_start <- 4

row_end <- 104

qty <- c(paste("BC",row_start,":","BJ",row_end, sep=""), 
         paste("BM",row_start,":","BT",row_end, sep=""), 
         paste("BW",row_start,":","CD",row_end, sep=""), 
         paste("CG",row_start,":","CN",row_end, sep=""), 
         paste("CQ",row_start,":","CX",row_end, sep=""), 
         paste("DA",row_start,":","DH",row_end, sep=""), 
         paste("DK",row_start,":","DR",row_end, sep=""), 
         paste("DU",row_start,":","EB",row_end, sep=""), 
         paste("EE",row_start,":","EL",row_end, sep=""),
         paste("EO",row_start,":","EV",row_end, sep=""),
         paste("EY",row_start,":","FF",row_end, sep=""),
         paste("FI",row_start,":","FP",row_end, sep=""),
         paste("FS",row_start,":","FZ",row_end, sep=""),
         paste("GC",row_start,":","GJ",row_end, sep=""),
         paste("GM",row_start,":","GT",row_end, sep=""),
         paste("GW",row_start,":","HD",row_end, sep=""),
         paste("HG",row_start,":","HN",row_end, sep=""),
         paste("HQ",row_start,":","HX",row_end, sep=""),
         paste("IA",row_start,":","IH",row_end, sep=""))

set <- cbind(hour, qty)

store <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "C4:D104") %>% as.data.frame()
colnames(store) <- c("store_code", "store_name")

date <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "D1:D3") %>% as.data.frame()

dflist <- list()
    
for (j in 1:nrow(set)) {
      
  tryCatch({
        
    df <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = set[j,2]) %>% as.data.frame() %>% 
          cbind(store) %>% gather(key = item, value = qty, -store_code, -store_name) %>%
          mutate(hour = set[j,1]) %>% mutate(date = date[1,])
        
    dflist[[j]] <- df
        
  }, error=function(e){cat("ERROR :",conditionMessage(e), "\n")})   
      
}
    
daily <- do.call(rbind, dflist) 
    
done <- paste(folder, "done", sep="/")
    
dir.create(done, showWarnings = FALSE, recursive = TRUE)
    
file.copy(paste(folder, wb[i], sep="/"), done)
    
file.remove(paste(folder, wb[i], sep="/"))
    
###NOTHING FOLLOWS