#KITCHEN DELIVERY FILES: FORMAT 4

row_start <- 4
row_end <- 84

qty <- c(paste("E",row_start,":","L",row_end, sep=""),
         paste("O",row_start,":","V",row_end, sep=""), 
         paste("Y",row_start,":","AF",row_end, sep=""),
         paste("AI",row_start,":","AP",row_end, sep=""),
         paste("AS",row_start,":","AZ",row_end, sep=""),
         paste("BC",row_start,":","BJ",row_end, sep=""), 
         paste("BM",row_start,":","BT",row_end, sep=""), 
         paste("BW",row_start,":","CD",row_end, sep=""), 
         paste("CG",row_start,":","CN",row_end, sep=""), 
         paste("CQ",row_start,":","CX",row_end, sep=""), 
         paste("DA",row_start,":","DH",row_end, sep=""), 
         paste("DK",row_start,":","DR",row_end, sep=""), 
         paste("DU",row_start,":","EB",row_end, sep=""), 
         paste("EE",row_start,":","EL",row_end, sep=""))

set <- cbind(hour, qty)

store <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "C4:D84") %>% as.data.frame()
colnames(store) <- c("store_code", "store_name")

date <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "D1:D3") %>% as.data.frame()

dflist <- list()
    
for (j in 1:nrow(set)) {
      
  tryCatch({
        
    df <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = set[j,2]) %>% as.data.frame() %>% 
          cbind(store) %>% gather(key = item, value = qty, -store_code, -store_name) %>%
          mutate(hour = set[j,1]) %>% mutate(date = date[1,])
        
    dflist[[j]] <- df
        
  }, error=function(e){cat("ERROR :",conditionMessage(e), "\n")})   
      
}
    
daily <- do.call(rbind, dflist) 
    
done <- paste(folder, "done", sep="/")
    
dir.create(done, showWarnings = FALSE, recursive = TRUE)
    
file.copy(paste(folder, wb[i], sep="/"), done)
    
file.remove(paste(folder, wb[i], sep="/"))

###NOTHING FOLLOWS