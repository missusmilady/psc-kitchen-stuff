kitchen <- "Molino"

folder <- paste(getwd(), "downloads", "molino", sep="/")

hour <- c(5:23)

wb <- list.files(path = folder, pattern = '*.xlsx', all.files = FALSE,
                 full.names = FALSE, recursive = FALSE,
                 ignore.case = FALSE, include.dirs = FALSE, no.. = FALSE)

datalist <- list()

for (i in 1:length(wb)) {
  
  check_time <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "BC1:BC2") %>% as.data.frame()
  check_col <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "IK1:IK2") %>% as.data.frame()
  check_row <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "D66:D67") %>% as.data.frame()
  
  if (nrow(check_time) > 0 && nrow(check_col) > 0 && nrow(check_row) > 0) {
    
    if (check_time[1,]=='4:30AM - 5:00AM' && 
        check_col[1,]=='TOTAL SERVED' && 
        check_row[1,]=='Mktg-Food Service') {
      
      row_start <- 4
      row_end <- 66
      
      source(paste(getwd(), "source", "format", "format1.R", sep="/"))
      
    }
    
  } else {
    
    check_time <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "BC1:BC2") %>% as.data.frame()
    check_col <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "IK1:IK2") %>% as.data.frame()
    check_row <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "D84:D85") %>% as.data.frame()
    
    if (nrow(check_time) > 0 && nrow(check_col) > 0 && nrow(check_row) > 0) {
      
      if (check_time[1,]=='4:30AM - 5:00AM' && 
          check_col[1,]=='TOTAL SERVED' && 
          check_row[1,]=='Mktg-Food Service') {
        
        row_start <- 4
        row_end <- 84
        
        source(paste(getwd(), "source", "format", "format1.R", sep="/"))
        
      }
      
    }
  }
  
  datalist[[i]] <- daily  
  
}

alldata <- do.call(rbind, datalist) %>% filter(qty > 0) %>% na.omit()

alldata$kitchen <- kitchen

molino <- alldata[,c("kitchen", "date", "hour", "store_code", 'item', "qty")]

###NOTHING FOLLOWS