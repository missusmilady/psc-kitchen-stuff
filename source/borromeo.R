kitchen <- "Borromeo"

folder <- paste(getwd(), "downloads", "borromeo", sep="/")

wb <- list.files(path = folder, pattern = '*.xlsx', all.files = FALSE,
                 full.names = FALSE, recursive = FALSE,
                 ignore.case = FALSE, include.dirs = FALSE, no.. = FALSE)

datalist <- list()

for (i in 1:length(wb)) {
  
  check_time <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "BC1:BC2") %>% as.data.frame()
  check_col <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "IK1:IK2") %>% as.data.frame()
  
  if (check_time[1,]=='4:30AM - 5:00AM' && 
      check_col[1,]=='TOTAL SERVED') {
    
      hour <- c(5:23)
      source(paste(getwd(), "source", "format", "format5.R", sep="/"))
    
  } else {
  
  check_time <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "E1:E2") %>% as.data.frame()
  check_col <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "IK1:IK2") %>% as.data.frame()
  check_row <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "D104:D105") %>% as.data.frame()
  
    if (check_time[1,]=='4:30AM - 5:00AM' && 
        check_col[1,]=='TOTAL SERVED' && 
        check_row[1,]=='Mktg-Food Service') {

      hour <- c(5:18)
      source(paste(getwd(), "source", "format", "format4.R", sep="/"))
      
    }
}
  
  daily$date <- as.Date(daily$date, format="%Y-%m-%d")
  
  datalist[[i]] <- daily  
  
}

alldata <- do.call(rbind, datalist) %>% filter(qty > 0) %>% na.omit()

alldata$kitchen <- kitchen

borromeo <- alldata[,c("kitchen", "date", "hour", "store_code", 'item', "qty")]

###NOTHING FOLLOWS