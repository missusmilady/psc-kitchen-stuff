kitchen <- "Cebu"

folder <- paste(getwd(), "downloads", "cebu", sep="/")

hour <- c(5:20)

wb <- list.files(path = folder, pattern = '*.xls', all.files = FALSE,
                 full.names = FALSE, recursive = FALSE,
                 ignore.case = FALSE, include.dirs = FALSE, no.. = FALSE)

datalist <- list()

for (i in 1:length(wb)) {
  
  check_time <- read_excel(paste(folder, wb[i], sep="/"), sheet="ORDER", range = "BC1:BC2") %>% as.data.frame()
 
  if (check_time[1,]=='4:30AM - 5:00AM') {
    
      row_start <- 4
      row_end <- 84
    
      source(paste(getwd(), "source", "format", "format6.R", sep="/"))
      
  }

datalist[[i]] <- daily  
    
}

alldata <- do.call(rbind, datalist) %>% filter(qty > 0) %>% na.omit()

alldata$kitchen <- kitchen

cebu <- alldata[,c("kitchen", "date", "hour", "store_code", 'item', "qty")]

###NOTHING FOLLOWS